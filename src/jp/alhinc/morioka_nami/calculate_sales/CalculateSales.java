package jp.alhinc.morioka_nami.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		//コマンドライン引数のチェック
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		
		Map<String, String> branchName = new HashMap<String, String>();
		Map<String, Long> branchSales = new HashMap<String, Long>();

		//支店処理ファイル読み込み処理
		BufferedReader bufferedReader = null;
		try {
			File file = new File(args[0], "branch.lst");
			
			if ( !file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			
			FileReader fr = new FileReader(file);
			bufferedReader = new BufferedReader(fr);

			String line;
			while ((line = bufferedReader.readLine()) != null) {

				String[] items = line.split(",");
				
				if ((items.length !=2) || (!items[0].matches("^[0-9]{3}$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				
				branchName.put(items[0], items[1]);
				branchSales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println("エラーが発生しました。");
			return;
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		//売上ファイル読み込み処理
		File file = new File(args[0]);
		File[] files = file.listFiles();

		List<File> rcdFiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();

			if (files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		
		Collections.sort(rcdFiles);
		
		//連番チェック
		for (int i = 0; i < rcdFiles.size() -1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			
			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		
		//抽出したファイルの読み込み、売上額等の抽出
		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				bufferedReader = new BufferedReader(new FileReader(rcdFiles.get(i)));

				ArrayList<String> fileContents = new ArrayList<>();
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					fileContents.add(line);
				}
				
				String fileName = rcdFiles.get(i).getName();
				
				//行数チェック
				if (fileContents.size() != 2) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}
				//支店コードの存在確認
				if(!branchName.containsKey(fileContents.get(0))) {
					System.out.println(fileName + "の支店コードが不正です");
				return;
				}
				//売上金額が数字かの確認
				if ( !fileContents.get(1).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				
				//支店コード
				String branchCode = fileContents.get(0);
				
				//売上金額の加算
				long fileSale = Long.parseLong(fileContents.get(1));
				Long saleAmount = branchSales.get(branchCode) + fileSale;

				//10桁未満であるか否か
				if (saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				
				branchSales.put(branchCode, saleAmount);

			} catch(IOException e) {
				System.out.println("エラーが発生しました。");
				return;
			} finally {
				if (bufferedReader != null) {
					try {
						bufferedReader.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		
		//集計結果の出力
		BufferedWriter bw = null;
		try {
			File file2 = new File(args[0], "branch.out");
			bw = new BufferedWriter(new FileWriter(file2)); 

			for (String key : branchName.keySet()) {
				bw.write(key + "," + branchName.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		} finally {
			try {
				if(bw != null) {
				bw.close();
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}
	}
}